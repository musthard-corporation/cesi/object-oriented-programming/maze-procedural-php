<?php

session_start();
$grid = $_SESSION['grid'];

[$playerRow, $playerColumn] = getPlayerPosition($grid);
[$exitRow, $exitColumn] = getExitPosition($grid);
[$nextRow, $nextColumn] = [$playerRow, $playerColumn];

switch ($_GET['direction']) {
    case 'up':
        if ($playerRow > 0 && $grid[$playerRow - 1][$playerColumn] !== '@') {
            $nextRow = $playerRow - 1;
        }
        break;
    case 'left':
        if ($playerColumn > 0 && $grid[$playerRow][$playerColumn - 1] !== '@') {
            $nextColumn = $playerColumn - 1;
        }
        break;
    case 'right':
        if ($playerColumn < count($grid) - 1 && $grid[$playerRow][$playerColumn + 1] !== '@') {
            $nextColumn = $playerColumn + 1;
        }
        break;
    case 'down':
        if ($playerRow < count($grid) - 1 && $grid[$playerRow + 1][$playerColumn] !== '@') {
            $nextRow = $playerRow + 1;
        }
        break;
}

if ($nextRow == $exitRow && $nextColumn == $exitColumn) {
    $grid[$playerRow][$playerColumn] = ' ';
    $grid[$nextRow][$nextColumn] = 'b';
} else if ($playerRow == $exitRow && $playerColumn == $exitColumn) {
    $grid[$playerRow][$playerColumn] = '"';
    $grid[$nextRow][$nextColumn] = '&';
} else {
    $grid[$playerRow][$playerColumn] = ' ';
    $grid[$nextRow][$nextColumn] = '&';
}

$_SESSION['grid'] = $grid;
header('Location: /index.php');

function getPlayerPosition($grid)
{
    for ($row = 0; $row < count($grid); $row++) {
        for ($column = 0; $column < count($grid); $column++) {
            if ($grid[$row][$column] == '&' || $grid[$row][$column] == 'b') {
                return [$row, $column];
            }
        }
    }

    return [-1, -1];
}

function getExitPosition($grid)
{
    for ($row = 0; $row < count($grid); $row++) {
        for ($column = 0; $column < count($grid); $column++) {
            if ($grid[$row][$column] == '"' || $grid[$row][$column] == 'b') {
                return [$row, $column];
            }
        }
    }

    return [-1, -1];
}
?>