<?php
session_start();

$grid = [
    ['&', ' ', ' ', ' ', ' '],
    ['@', '@', '@', '@', ' '],
    [' ', ' ', ' ', ' ', ' '],
    ['@', ' ', '@', '@', '@'],
    [' ', ' ', ' ', ' ', '"'],
];

if (!isset($_GET['reset']) && isset($_SESSION['grid'])) {
    $grid = $_SESSION['grid'];
} else {
    $_SESSION['grid'] = $grid;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Maze</title>
    <link rel="stylesheet" href="/css/site.css" />
</head>

<body>
    <main>
        <div class="maze" style="--size: <?php echo count($grid) ?>">
            <?php for ($row = 0; $row < count($grid); $row++) { ?>
                <div class="row">
                    <?php for ($column = 0; $column < count($grid); $column++) { ?>
                        <div class="column">
                            <?php if ($grid[$row][$column] == '"' || $grid[$row][$column] == 'b') { ?>
                                <img class="player" src="/image/exit.png">
                            <?php } ?>
                            <?php if ($grid[$row][$column] == '&' || $grid[$row][$column] == 'b') { ?>
                                <img class="player" src="/image/player.png">
                            <?php } ?>
                            <?php if ($grid[$row][$column] == '@') { ?>
                                <img class="player" src="/image/wall.png">
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="actions">
            <div class="move-pad">
                <div class="direction">
                    <a class="button" href="/move.php?direction=up">⇧</a>
                </div>
                <div class="horizontal">
                    <div class="direction">
                        <a class="button" href="/move.php?direction=left">⇦</a>
                    </div>
                    <div class="direction empty"></div>
                    <div class="direction">
                        <a class="button" href="/move.php?direction=right">⇨</a>
                    </div>
                </div>
                <div class="direction">
                    <a class="button" href="/move.php?direction=down">⇩</a>
                </div>
            </div>
            <div class="game-actions">
                <a class="button reset" href="/?reset">Réinitialiser</a>
            </div>
        </div>
    </main>
    <script>
        window.onkeydown = (event) => {
            switch (event.keyCode) {
                case 37:
                    location.href = '/move.php?direction=left';
                    break;
                case 38:
                    location.href = '/move.php?direction=up';
                    break;
                case 39:
                    location.href = '/move.php?direction=right';
                    break;
                case 40:
                    location.href = '/move.php?direction=down';
                    break;
            }
        }
    </script>

    <script src="/js/site.js"></script>
</body>

</html>